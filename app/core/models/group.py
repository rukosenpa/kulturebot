from django.db import models


class Group(models.Model):
    title = models.CharField(verbose_name='Название', max_length=50)
    # parent_group = models.ForeignKey('self', verbose_name='Основная группа', related_name='children', blank=True,
    #                                  null=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        return self.title

    def as_button(self):
        return self.title
