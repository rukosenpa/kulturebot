from django.db import models


class DishAmount(models.Model):
    customer = models.ForeignKey('core.Customer', on_delete=models.CASCADE, null=True)
    product = models.ForeignKey('core.Dish', on_delete=models.CASCADE, null=True)
    amount = models.IntegerField(default=1)

    class Meta:
        unique_together = ('customer', 'product')
