from django.db import models

from core.models.mixins import CartMixin

SOCIAL_CHOICES = (
    ('Telegram', 'Телеграмм'),
    ('VK', 'ВК'),
)


class Customer(CartMixin, models.Model):
    social_name = models.CharField(choices=SOCIAL_CHOICES, max_length=50)

    social_id = models.BigIntegerField(unique=True)

    first_name = models.CharField(verbose_name='Имя', max_length=50)
    last_name = models.CharField(verbose_name='Фамилия', max_length=50)

    class Meta:
        verbose_name = "Покупатель"
        verbose_name_plural = "Покупатели"

    def __str__(self):
        return self.first_name + ' ' + self.last_name
