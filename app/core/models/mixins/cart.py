from django.db import models, IntegrityError
from django.db.models import F

from core.models.dish import Dish
from core.models.dish_in_cart_amount import DishAmount


class CartMixin(models.Model):
    class Meta:
        abstract = True

    products = models.ManyToManyField('core.Dish', through='core.DishAmount', related_name='customers')

    def add_dish(self, dish: Dish):
        try:
            DishAmount.objects.create(customer=self, product=dish)
        except IntegrityError:
            DishAmount.objects.filter(customer=self, product=dish).update(amount=F('amount') + 1)

    def delete_dish(self, dish: Dish, amount: int):
        try:
            dish_with_amount = DishAmount.objects.get(customer=self, product=dish)
            if dish_with_amount.amount < amount:
                self.products.delete(dish)
                self.save()
            else:
                dish_with_amount.amount -= amount
                dish_with_amount.save()
        except DishAmount.DoesNotExist:
            pass
