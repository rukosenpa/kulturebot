from django.db import models
from django.utils import timezone


class Order(models.Model):
    user = models.ForeignKey('core.Customer', on_delete=models.PROTECT)
    total = models.DecimalField('Сумма заказа', max_digits=7, decimal_places=2)

    arrival_at = models.DateTimeField(verbose_name='Время прихода', default=timezone.now)
    processed = models.BooleanField(verbose_name='Обработан')

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    def __str__(self):
        return f"{self.user}({self.arrival_at}) на сумму {self.total}"
