from django.db import models


class Dish(models.Model):
    title = models.CharField(verbose_name='Название', max_length=50)

    # TODO: replace parent dish with option model for dish

    category = models.ForeignKey('core.Group', verbose_name='Группа', related_name='dishes', null=True,
                                 on_delete=models.PROTECT, blank=True)

    photo = models.ImageField('Фотография', upload_to='dishes', null=True, blank=True)
    price = models.DecimalField(verbose_name='Цена', max_digits=7, decimal_places=2, null=True, blank=True)
    _text_description = models.TextField('Описание', blank=True)

    class Meta:
        verbose_name = "Блюдо"
        verbose_name_plural = "Блюда"

    def __str__(self):
        return self.title

    @property
    def description(self):
        return "\n".join([self.title, self._text_description, str(self.price)])
