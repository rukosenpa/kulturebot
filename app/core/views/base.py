import json

from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse, HttpRequest
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from core.messengers.payloads.base import BasePayload


class CallbackView(View):
    payload_class = None
    success_response_to_messenger = None

    def __init__(self, success_response_to_messenger, payload_class, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if payload_class is None:
            raise ImproperlyConfigured("payload_class can't be None")

        self.payload_class = payload_class
        self.success_response_to_messenger = success_response_to_messenger

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request: HttpRequest):
        self.payload: BasePayload = self.payload_class(json.loads(request.body))

        command = self.payload.command
        command.apply()

        response = HttpResponse(request, status=200)
        response.content = self.success_response_to_messenger
        return response
