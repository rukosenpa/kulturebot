from django.urls import path

from core.messengers.payloads import TelegramPayload
from core.views.base import CallbackView

app_name = 'bot-callbacks'

urlpatterns = [
    # path('vk/', ),
    path('telegram/', CallbackView.as_view(payload_class=TelegramPayload), name='telegram'),
]
