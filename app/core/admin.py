from django.contrib import admin

from core.models import Customer, Dish, Group, Order


@admin.register(Customer)
class CustomerModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Dish)
class DishModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Group)
class GroupModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Order)
class OrderModelAdmin(admin.ModelAdmin):
    pass
