# Generated by Django 3.0.6 on 2020-08-10 09:54

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    replaces = [('core', '0001_initial'), ('core', '0002_auto_20200728_1606'), ('core', '0003_dish_category'), ('core', '0004_auto_20200728_1648'), ('core', '0005_auto_20200728_1649'), ('core', '0006_auto_20200728_1649'), ('core', '0007_remove_group_parent_group'), ('core', '0008_auto_20200803_1834'), ('core', '0009_auto_20200804_1641'), ('core', '0010_auto_20200810_1620')]

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('social_name', models.CharField(choices=[('Telegram', 'Телеграмм'), ('VK', 'ВК')], max_length=50)),
                ('social_id', models.BigIntegerField(unique=True)),
                ('first_name', models.CharField(max_length=50, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=50, verbose_name='Фамилия')),
            ],
            options={
                'verbose_name': 'Покупатель',
                'verbose_name_plural': 'Покупатели',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='Сумма заказа')),
                ('arrival_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Время прихода')),
                ('processed', models.BooleanField(verbose_name='Обработан')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.Customer')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Группа',
                'verbose_name_plural': 'Группы',
            },
        ),
        migrations.CreateModel(
            name='Dish',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True, verbose_name='Цена')),
                ('parent_dish', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='core.Dish', verbose_name='Основа для позиции')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dishes', to='core.Group', verbose_name='Группа')),
                ('_text_description', models.TextField(blank=True, verbose_name='Описание')),
                ('photo', models.ImageField(blank=True, null=True, upload_to='dishes', verbose_name='Фотография')),
            ],
            options={
                'verbose_name': 'Блюдо',
                'verbose_name_plural': 'Блюда',
            },
        ),
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='cart', to='core.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='DishAmount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.IntegerField(default=1)),
                ('cart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Cart')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Dish')),
            ],
            options={
                'unique_together': {('cart', 'product')},
            },
        ),
        migrations.AddField(
            model_name='cart',
            name='products',
            field=models.ManyToManyField(related_name='carts', through='core.DishAmount', to='core.Dish'),
        ),
    ]
