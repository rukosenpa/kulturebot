from abc import ABC, abstractmethod
from collections import namedtuple

from django.core.exceptions import ImproperlyConfigured

from core.messengers.commands import UnknownCommand, COMMANDS_CLASS_MAPPING
from core.messengers.commands.base import Command
from core.models import Customer

PayloadData = namedtuple('PayloadData', ('action', 'credentials', 'data'))


class BasePayload(ABC):
    _customer_social_name = None
    messenger_api_adapter = None

    def __init__(self, payload: dict):
        self.payload_data: PayloadData = self._obtain_data(payload)

        # Obtain customer
        if self._customer_social_name is None:
            raise ImproperlyConfigured("Property _customer_social_name must be set")

        costumer_credentials = self.payload_data.credentials
        self.customer, _ = Customer.objects.get_or_create(
            social_id=costumer_credentials['id'],
            social_name=self._customer_social_name,
            first_name=costumer_credentials['first_name'],
            last_name=costumer_credentials['last_name']
        )

        # Obtain command
        if self.messenger_api_adapter is None:
            raise ImproperlyConfigured("Property messenger_api_adapter must be set")

        action = self.payload_data.action
        command_class = COMMANDS_CLASS_MAPPING.get(action, UnknownCommand)
        self.command: Command = command_class(self.customer, self.payload_data.data, self.messenger_api_adapter)

    @abstractmethod
    def _obtain_data(self, payload: dict) -> PayloadData:
        pass
