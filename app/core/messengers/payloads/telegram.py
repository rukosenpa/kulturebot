import json

from core.messengers.adapters.telegram import TelegramAdapter
from core.messengers.commands.exceptions import PayloadMappingError
from core.messengers.payloads.base import BasePayload, PayloadData


class TelegramPayload(BasePayload):
    _customer_social_name = 'telegram'
    messenger_api_adapter = TelegramAdapter

    def _obtain_data(self, payload: dict) -> PayloadData:
        # Callback query branch
        try:
            if 'callback_query' in payload:
                callback_query = payload['callback_query']

                customer_credentials = callback_query['from']
                callback_query_data = json.loads(callback_query['data'])
                action = callback_query_data.pop('action')

                callback_query_data['callback_query_id'] = payload['callback_query']['id']

                return PayloadData(action, customer_credentials, callback_query_data)

            # Buttons and commands branch
            elif 'message' in payload:
                customer_credentials = payload['message']['from']
                message_text = payload['message']['text']

                if message_text == '/start':
                    action = 'start'
                elif message_text == 'Menu':
                    action = 'list_menu'
                else:
                    action = 'unknown'

                return PayloadData(action, customer_credentials, {})

            return PayloadData('', {}, {})

        except KeyError as e:
            raise PayloadMappingError from e
