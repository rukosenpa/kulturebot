from .telegram import TelegramPayload

__all__ = [
    'TelegramPayload',
]
