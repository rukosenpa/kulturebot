from abc import ABC, abstractmethod
from typing import Sequence

from core.models import Dish, Group, Customer


class BaseAdapter(ABC):

    @property
    @abstractmethod
    def api_object(self):
        pass

    @classmethod
    @abstractmethod
    def send_controls(cls, customer: Customer, buttons):
        pass

    @classmethod
    @abstractmethod
    def _send_dish(cls, customer: Customer, dish: Dish) -> None:
        pass

    @classmethod
    @abstractmethod
    def send_group(cls, customer: Customer, group: Group) -> None:
        pass

    @classmethod
    @abstractmethod
    def send_message(cls, customer: Customer, text: str, reply_markup=None) -> None:
        pass

    @classmethod
    @abstractmethod
    def send_menu(cls, customer: Customer, menu: Sequence[Group]) -> None:
        pass

    @classmethod
    @abstractmethod
    def answer_callback_query(cls, callback_query_id: str, text: str, show_alert: bool = False):
        pass
