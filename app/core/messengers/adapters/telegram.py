from typing import List

from constance import config
from django.db.models import QuerySet
from telebot import TeleBot
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup

from core.messengers.adapters.base import BaseAdapter
from core.messengers.commands import AddCommand, GetCommand
from core.models import Customer, Dish, Group


class TelegramAdapter(BaseAdapter):
    api_object = TeleBot(config.TELEGRAM_BOT_TOKEN)

    @classmethod
    def send_controls(cls, customer: Customer, buttons: List):
        main_keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
        for row in buttons:
            if isinstance(row, str):
                main_keyboard.add(row)
            else:
                main_keyboard.add(*row)

        cls.api_object.send_message(
            chat_id=customer.social_id,
            text=config.GREETINGS,
            reply_markup=main_keyboard
        )

    @classmethod
    def _send_dish(cls, customer: Customer, dish: Dish) -> None:
        buttons_markup: InlineKeyboardMarkup = InlineKeyboardMarkup()
        add_button = InlineKeyboardButton('Добавить', callback_data=AddCommand.as_callback_data(dish.id))
        buttons_markup.add(add_button)

        cls.api_object.send_photo(
            chat_id=customer.social_id,
            photo=dish.photo,
            caption=dish.description,
            reply_markup=buttons_markup
        )

    @classmethod
    def send_group(cls, customer: Customer, group: Group) -> None:
        if not group.dishes.exists():
            cls.api_object.send_message(
                customer.social_id,
                'Извините но пока в этой группе отсутствуют товары'
            )
        else:
            for dish in group.dishes.all():
                cls._send_dish(customer, dish)

    @classmethod
    def send_message(cls, customer: Customer, text: str, reply_markup=None) -> None:
        cls.api_object.send_message(
            chat_id=customer.social_id,
            text=text,
            reply_markup=reply_markup
        )

    @classmethod
    def send_menu(cls, customer: Customer, menu: QuerySet) -> None:
        buttons_markup: InlineKeyboardMarkup = InlineKeyboardMarkup(row_width=2)
        buttons_markup.add(*[InlineKeyboardButton(
            text=group.title,
            callback_data=GetCommand.as_callback_data(group.id)
        ) for group in menu])

        cls.api_object.send_message(
            chat_id=customer.social_id,
            text=config.ACCOMPANYING_MENU_MESSAGE,
            reply_markup=buttons_markup
        )

    @classmethod
    def answer_callback_query(cls, callback_query_id: str, text: str, show_alert: bool = False):
        cls.api_object.answer_callback_query(
            callback_query_id=callback_query_id,
            text=text,
            show_alert=show_alert
        )
