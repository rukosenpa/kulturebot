import json

from sentry_sdk import capture_exception
from telebot.apihelper import ApiException

from core.messengers.commands.base import Command
from core.messengers.commands.mixins.inline_button import InlineButtonMixin
from core.models import Dish


class AddCommand(InlineButtonMixin, Command):
    def apply(self):
        try:
            dish: Dish = Dish.objects.get(id=self.data['dish_id'])

            self.customer.add_dish(dish)

            try:
                self.messenger_api.answer_callback_query(
                    callback_query_id=self.data['callback_query_id'],
                    text=f"{dish} теперь в вашем заказе",
                )
            except ApiException as e:
                self.messenger_api.send_message(
                    customer=self.customer,
                    text=f"{dish} теперь в вашем заказе",
                )
                capture_exception(e)

        except Dish.DoesNotExist:
            self.messenger_api.answer_callback_query(
                callback_query_id=self.data['callback_query_id'],
                text="Данное блюдо сейчас недоступно",
            )

    @staticmethod
    def as_callback_data(object_id: int):
        return json.dumps({'action': 'add', 'dish_id': object_id})
