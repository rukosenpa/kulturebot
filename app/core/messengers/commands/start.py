from constance import config

from core.messengers.commands.base import Command


class StartCommand(Command):
    def apply(self):
        self.messenger_api.send_controls(
            self.customer,
            [
                ['Menu', 'Order'],
                ['Cancel', 'Cart']
            ]
        )
