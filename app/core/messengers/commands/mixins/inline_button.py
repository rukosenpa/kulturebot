from abc import ABC, abstractmethod


class InlineButtonMixin(ABC):
    @staticmethod
    @abstractmethod
    def as_callback_data(object_id: int):
        pass
