from .add import AddCommand
from .get import GetCommand
from .list_menu import ListMenuCommand
from .start import StartCommand
from .unknown_command import UnknownCommand

__all__ = [
    'AddCommand',
    'GetCommand',
    'StartCommand',
    'ListMenuCommand',
    'UnknownCommand',

    'COMMANDS_CLASS_MAPPING'
]

COMMANDS_CLASS_MAPPING = {
    'add': AddCommand,
    'get': GetCommand,
    'list_menu': ListMenuCommand,
    'start': StartCommand,
}
