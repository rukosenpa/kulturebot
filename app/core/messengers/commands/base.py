from abc import ABC, abstractmethod

from core.messengers.adapters.base import BaseAdapter
from core.models import Customer


# PATTERN "Command" realize here
# Instantiating by Payload object
class Command(ABC):
    def __init__(self, customer: Customer, data: dict, messenger_api: BaseAdapter):
        self.customer = customer
        self.data = data
        self.messenger_api = messenger_api

    @abstractmethod
    def apply(self):
        pass
