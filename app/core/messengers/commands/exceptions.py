class CommandApplyingError(Exception):
    pass


class PayloadMappingError(Exception):
    pass
