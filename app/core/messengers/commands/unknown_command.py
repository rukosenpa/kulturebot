from core.messengers.commands.base import Command


class UnknownCommand(Command):
    def __init__(self):
        super().__init__(None, None)

    def apply(self):
        return "Извините, но пока я не могу вас понять :("

    @staticmethod
    def as_callback_data(object_id: int):
        pass
