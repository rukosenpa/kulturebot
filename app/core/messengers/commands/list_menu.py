from core.messengers.commands.base import Command
from core.models import Group


class ListMenuCommand(Command):
    def apply(self):
        menu = Group.objects.all()
        if len(menu):
            self.messenger_api.send_menu(self.customer, menu)

        else:
            self.messenger_api.send_message(
                self.customer,
                "В данный момент в меню нет ни одного блюда"
            )
