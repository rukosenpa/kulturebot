import json

from core.messengers.commands.base import Command
from core.messengers.commands.mixins.inline_button import InlineButtonMixin
from core.models import Group


class GetCommand(InlineButtonMixin, Command):
    def apply(self):
        try:
            group = Group.objects.get(id=self.data['group_id'])

            self.messenger_api.send_group(self.customer, group)

        except Group.DoesNotExist:
            self.messenger_api.answer_callback_query(
                callback_query_id=self.data['callback_query_id'],
                text='Данная группа блюд сейчас недоступна'
            )

    @staticmethod
    def as_callback_data(object_id: int):
        return json.dumps({'action': 'get', 'group_id': object_id})
