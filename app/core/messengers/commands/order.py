from core.messengers.commands.base import Command


class OrderCommand(Command):
    def apply(self):
        self.messenger_api.send_message(self.customer, "Error while creating order")

    @staticmethod
    def as_callback_data(object_id: int):
        pass
