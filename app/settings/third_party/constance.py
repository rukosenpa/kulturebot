__all__ = [
    'CONSTANCE_BACKEND',
    'CONSTANCE_CONFIG',
]

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_CONFIG = {
    'TELEGRAM_BOT_TOKEN': ('', 'Token for bot authenticating on Telegram servers', str),
    # 'VkBotToken': {},

    'GREETINGS': ('Добрый день', 'Сообщение с которым бот будет привествовать пользователя', str),
    'ACCOMPANYING_MENU_MESSAGE': ('Вот наше меню', 'Текст сообщения, приходящий вместе с меню', str),

    'MENU_BUTTON': ('Меню', '', str),
    'ORDER_BUTTON': ('Сделать заказ', '', str),
    'CART_BUTTON': ('Корзина', '', str),
    'CANCEL_BUTTON': ('Отменить заказ', '', str),
}
