import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from settings.django.base import env

sentry_sdk.init(
    dsn=env.str('SENTRY_DSN', default=''),
    integrations=[DjangoIntegration()],

    send_default_pii=True
)
