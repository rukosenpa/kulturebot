from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('callbacks/', include('core.urls', namespace='bot-callbacks')),
]
